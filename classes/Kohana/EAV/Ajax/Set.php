<?php defined('SYSPATH') or die('No direct script access.'); 

/**
 * ORM Model EAV_Acounting_Set
 *
 * Created on 2014-04-08
 */

class Kohana_EAV_Ajax_Set extends Kohana_EAV_Set {
	public function to_ajax($data = array())
	{
		$data["model"] = "eav_set";
		$data["values"] = $this->_object;
		return $data;
	}
	
}
?>