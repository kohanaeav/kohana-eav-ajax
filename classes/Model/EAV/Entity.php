<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_EAV_Entity extends Kohana_EAV_Ajax_Entity {

	public function to_ajax($data = array())
	{
		$data["model"] = $this->_model;
		$data["set"] = $this->set->to_ajax();
		$data["values"] = $this->_object;
		foreach ($this->_entity_attributes_values as $key => $value)
		{
			$data["values"][$key] = $value->to_ajax();
		}
		return $data;
	}

	public function to_autocomplete($data = array())
	{
		$data["object"] = $this->to_ajax();
		$data["guid"] = $this->entity_guid;
		$data["name"] = $this->entity_name;
		$data["value"] = $this->entity_name; // to be deprecated
		$data["url"] = $this->get_url(); // to be deprecated
		$data["profile_url"] = $this->get_url();
		//$data["image_url"] = $object->get_url("/profilepicture"); // to be implemented
		return $data;
	}

}
